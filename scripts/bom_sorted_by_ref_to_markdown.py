#
# Example python script to generate a BOM from a KiCad generic netlist
#
# Example: Ungrouped (One component per row) markdown output
#

"""
    @package
    Generate a csv list file.
    Components are sorted by ref
    One component per line
    Fields are (if exist)
    Ref, value, Part, footprint, Datasheet, Manufacturer, Vendor

    Command line:
    python "pathToFile/bom_csv_sorted_by_ref.py" "%I" "%O.csv"
"""

from __future__ import print_function

# Import the KiCad python helper module
import kicad_netlist_reader
import sys

# Generate an instance of a generic netlist, and load the netlist tree from
# the command line option. If the file doesn't exist, execution will stop
NETLIST_FILE = "../kicad-pcb/4pstat.xml"
#OUTPUT_FILE = #"../kicad-pcb/4pstat_BOM.md"
FIELD_NAMES = ['Ref', 'Value', 'Footprint', 'Description', 'Digi-Key_PN','DK_Detail_Page']
if len(sys.argv) >= 2:
    NETLIST_FILE = sys.argv[1]

net = kicad_netlist_reader.netlist(NETLIST_FILE)

# Open a file to write to, if the file cannot be opened output to stdout
# instead
f = sys.stdout
if len(sys.argv) >= 3:
    OUTPUT_FILE = sys.argv[2]
    try:
        f = open(OUTPUT_FILE, 'w')
    except IOError:
        e = "Can't open output file for writing: " + OUTPUT_FILE
        print( __file__, ":", e, sys.stderr )


components = net.getInterestingComponents()

#print("***DEBUG***");import IPython;IPython.embed()
# Output a field delimited header line
f.write(f"```\n")
f.write(f"Source: {net.getSource()}\n")
f.write(f"Date: {net.getDate()}\n")
f.write(f"Tool: {net.getTool()}\n")
f.write(f"Component Count: {len(components)}\n")
fieldstr = " ".join(FIELD_NAMES)
f.write(f"Fields: {fieldstr}\n")
f.write(f"```\n")
f.write(f"Components:\n")

# Output all of the component information (One component per row)
for c in components:
    ref = c.getRef()
    val = c.getValue()
    fp  = c.getFootprint()
    desc = c.getField("Description")
    dk_pn = c.getField("Digi-Key_PN")
    dk_url = c.getField("DK_Detail_Page")
    dk_link = ""
    if not dk_pn:
        dk_pn = "digikey_details"
    if dk_url:
        dk_link = f"[{dk_pn}]({dk_url})"
    if not desc:
        desc = "no part selected"
    f.write(f"- {ref}, {val}, {fp}, {desc}, {dk_link}\n")
    #writerow( out, [c.getRef(), c.getValue(), c.getFootprint(), c.getDatasheet(),
    #    c.getField("Manufacturer"), c.getField("Vendor")])

